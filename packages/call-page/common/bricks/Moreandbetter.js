import React from 'react'
import { styled } from "frontity"
import { Container } from '../styles'
import MiniForm from './../mini-form/Form'

const Article = styled.article`
    position: relative;
    padding-top: ${props => props.marginDesktop ? props.marginDesktop : 'clamp(140px, 12vw, 240px);'};
    @media(max-width: 1198px){
        padding-top: ${props => props.marginTablet ? props.marginTablet : 'clamp(90px, 12vw, 140px);'};
    }
    @media(max-width: 764px){
        padding-top: ${props => props.marginPhone ? props.marginPhone : '90px'};
    }
`

const Flex = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    @media(max-width: 1196px){
        flex-direction: column-reverse;
    }
`

const LeftColumn = styled.div`
    max-width: 33%;
    padding-right: 1%;
    @media(max-width: 1196px){
        padding: 0; 
        max-width: 800px;
        width: 100%;
    }
`

const TitleWrapper = styled.div`
    display: flex;
    align-items: center;
    margin-bottom: 15px;
`

const Img = styled.img`
    margin-right: 30px;
`

const Title = styled.h2`
    margin: 0 0 110px;
    text-align: center;
    font-size: clamp(45px, 3.5vw, 60px);
    line-height: clamp(60px, 3.5vw, 80px);
    font-weight: bold;
    @media(max-width: 1196px){
        font-size: 45px;
        line-height: 60px;
        margin-top: 180px;
        margin-bottom: 80px;
    }
    @media(max-width: 764px){
        font-size: 28px;
        line-height: 40px;
        margin-top: 120px;
    }
`

const ListTitle = styled.h3`
    font-size: 28px;
    line-height: 40px;
    font-weight: bold;
    @media(max-width: 1196px){
        font-size: 22px;
        line-height: 30px;
    }
    @media(max-width: 764px){
        font-size: 18px;
        line-height: 28px;
    }
`

const List = styled.ul`

`

const ListItem = styled.li`
    margin-bottom: 90px;

    @media(max-width: 1196px){
        margin-bottom: 60px;
    }
    @media(max-width: 764px){
        margin-bottom: 30px;
    }

    :last-child {
        margin-bottom: 0;
    }
`

const TextWrapper = styled.p`
    color: #6E7276;
    font-size: 16px;
    line-height: 24px;
    @media(max-width: 764px){
        font-size: 14px;
        line-height: 20px;
    }
`

const RightColumn = styled.div`
    max-width: 66%;
    @media(max-width: 1196px){
        max-width: 800px;
        width: 100%;
    }
`

const ImgBig = styled.img`
    max-width: 100%;
    height: auto;
`

const FormMain = styled.div`
max-width: 1080px;
margin: 0 auto;;
`

const Moreandbetter = (props) => {

    return (
        <Article marginDesktop={props.marginDesktop} marginTablet={props.marginTablet} marginPhone={props.marginPhone}>
            <Container>
                <Title>{props.acf.title}</Title>
                <Flex>
                    <LeftColumn>
                        <List>
                            {props.acf.repeater.map((el, index) =>
                                <ListItem key={index}>
                                    <TitleWrapper>
                                        <Img alt={el.img_alt} src={el.img} />
                                        <ListTitle>{el.title}</ListTitle>
                                    </TitleWrapper>
                                    <TextWrapper>{el.text}</TextWrapper>
                                </ListItem>
                            )}
                        </List>
                    </LeftColumn>
                    <RightColumn>
                        <ImgBig alt={props.acf.img_alt} src={props.acf.img} />
                    </RightColumn>
                </Flex>


                <FormMain>
                    {
                        props.acf.form
                            ?
                            <MiniForm acf={props.acf.form} />

                            : null
                    }
                </FormMain>
            </Container>
        </Article>
    )
}

export default Moreandbetter