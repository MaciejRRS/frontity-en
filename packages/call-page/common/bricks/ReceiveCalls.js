import React from 'react'
import { styled } from "frontity"
import { Container, Flex } from '../styles'

const Article = styled.article`
    position: relative;
    padding-top: ${props => props.marginDesktop ? props.marginDesktop : 'clamp(140px, 12vw, 240px);'};
    @media(max-width: 1198px){
        padding-top: ${props => props.marginTablet ? props.marginTablet : 'clamp(90px, 12vw, 140px);'};
    }
    @media(max-width: 764px){
        padding-top: ${props => props.marginPhone ? props.marginPhone : '90px'};
    }
`

const LocContainer = styled(Container)`
    max-width: 1360px;
`

const TextPart = styled.div`
    max-width: 520px;
    padding-left: 30px;
    @media(max-width: 1198px){
        max-width: 665px;
        width: 100%;
        text-align: center;
        padding-left: 0;
    }
`

const ImgPart = styled.div`
    max-width: 665px;
    width: 100%;

    img{
        width: 100%;
    }
`

const Title = styled.h2`
    font-size: 45px;
    line-height: 60px;
    font-weight: bold;
    padding-bottom: 20px;
    @media(max-width: 1198px){
        font-size: 28px; 
        line-height: 40px;
    }
    @media(max-width: 764px){
        font-size: 22px;
        line-height: 30px;
    }
`

const Text = styled.p`
    @media(max-width: 1198px){
        padding-bottom: 30px;
    }
    @media(max-width: 764px){
        font-size: 14px;
        line-height: 30px;
    }
`

const LocFlex = styled(Flex)`
    @media(max-width: 1198px){
        flex-direction: column-reverse;
    }
`

const ReceiveCalls = (props) => {
    return (
        <Article marginDesktop={props.marginDesktop} marginTablet={props.marginTablet} marginPhone={props.marginPhone}>
            <LocContainer>
                <LocFlex>
                    <ImgPart>
                        <img alt={props.acf.img_alt} src={props.acf.img} />
                    </ImgPart>
                    <TextPart>
                        <Title>{props.acf.title}</Title>
                        <Text>{props.acf.text}</Text>
                    </TextPart>
                </LocFlex>
            </LocContainer>
        </Article>
    )
}

export default ReceiveCalls