import React from 'react'
import { styled } from "frontity"
import { Container, Text } from '../styles'
import MiniForm from '../mini-form/Form'

const Article = styled.article`
    position: relative;
    padding-top: ${props => props.marginDesktop ? props.marginDesktop : '160px'};
    @media(max-width: 1198px){
        padding-top: ${props => props.marginTablet ? props.marginTablet : '60px'};
    }
    @media(max-width: 764px){
        padding-top: ${props => props.marginPhone ? props.marginPhone : '60px'};
    }
`

const Title = styled.h1`
    text-align: center;
    font-size: clamp(45px, 3.5vw, 60px);
    line-height: clamp(60px, 3.5vw, 80px);
    font-weight: bold;
    max-width: 900px;
    margin: 0 auto;
    @media(max-width: 1198px){
        font-size: 45px;
        line-height: 60px;
    }  
    @media(max-width: 764px){
        font-size: 28px;
        line-height: 40px;
    }
`

const LocText = styled(Text)`
    max-width: 1082px;
    font-size: 28px;
    line-height: 40px;
    margin: 40px auto 60px;
    text-align: center;
    @media(max-width: 1198px){
        font-size: 22px;
        line-height: 30px;
    }  
    @media(max-width: 764px){
        font-size: 18px;
        line-height: 28px;
    }
`

const MoreEffective = (props) => {
    return (
        <Article marginDesktop={props.marginDesktop} marginTablet={props.marginTablet} marginPhone={props.marginPhone}>
            <Container>
                <Title>{props.acf.title}</Title>
                <LocText>{props.acf.text}</LocText>
                <MiniForm acf={props.acf.form} />
            </Container>
        </Article>
    )
}

export default MoreEffective