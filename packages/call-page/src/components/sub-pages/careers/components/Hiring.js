import React from 'react'
import { Container } from '../../../../../common/styles'
import { styled } from "frontity"
import ReactHtmlParser from 'react-html-parser';

const Article = styled.article`
    position: relative;
    padding-top: ${props => props.marginDesktop ? props.marginDesktop : 'clamp(140px, 12vw, 240px);'};
    @media(max-width: 1198px){
        padding-top: ${props => props.marginTablet ? props.marginTablet : 'clamp(90px, 12vw, 140px);'};
    }
    @media(max-width: 764px){
        padding-top: ${props => props.marginPhone ? props.marginPhone : '90px'};
    }
`

const Title = styled.h2`
    text-align: center;
    font-size: clamp(45px, 3.5vw, 60px);
    line-height: clamp(60px, 3.5vw, 80px);
    font-weight: bold;
    padding: 0 0 120px;
    @media(max-width: 1198px){
        padding-bottom: 60px;
        font-size: 45px;
        line-height: 60px;
    }
    @media(max-width: 764px){
        padding-bottom: 60px;
        font-size: 28px;
        line-height: 40px;
    }
`

const Grid = styled.div`
display: grid;
grid-template-columns: 1fr 1fr 1fr;
gap: 30px;

 @media (max-width: 1198px) {
    grid-template-columns: 1fr;

} 
`


const Flex = styled.div`
display: flex;
align-items: center;
margin-top: -15px;
margin-bottom: 30px;

@media (max-width: 1198px) {
    margin-top: 0;
} 
`

const Item = styled.div`
box-shadow: 0 3px 6px #00000016;
border-radius: 12px;
padding: 0 15px 30px;

@media (max-width: 1198px) {
    margin: 0 auto;
    max-width: 510px;
    padding: 15px 30px 30px 15px;

} 

img {
    max-width: 120px;
    height: auto;
    margin-right: 45px;

    @media (max-width: 1198px) {
        max-width: 90px;
        margin-right: 30px;
    }
}

a {
    color: #377DFF;
    transition: .2s linear;
    &:hover {
        color: #ff7640;
    }
}
`

const TitleItem = styled.h3`
font-size: 28px;
line-height: 40px;
font-weight: bold;

@media (max-width: 1198px) {
    font-size: 22px;
    line-height: 30px;

} 
`

const Text = styled.p`
font-size: 16px;
line-height: 24px;
color: #6E7276;

@media (max-width: 1198px) {
    padding: 0 15px;
    font-size: 14px;
    line-height: 20px;
} 
`

const Branchers = (props) => {
    return (
        <Article marginDesktop={props.marginDesktop} marginTablet={props.marginTablet} marginPhone={props.marginPhone}>
            <Container>
                <Title>{props.acf.title}</Title>
                <Grid>
                    {
                        props.acf.repeater.map((el, index) =>
                            <Item key={index}>
                                <Flex>
                                    <img alt={el.img_alt} src={el.img} />
                                    <TitleItem>{el.title}</TitleItem>
                                </Flex>
                                <Text>{ReactHtmlParser(el.text)}</Text>
                            </Item>
                        )
                    }
                </Grid>
            </Container>
        </Article>
    )
}

export default Branchers