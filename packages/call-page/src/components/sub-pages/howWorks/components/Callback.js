import React from 'react'
import { styled } from "frontity"
import { Container } from '../../../../../common/styles'



const Article = styled.article`
    padding-top: 180px;
    margin-top: -60px;

    @media(max-width: 1198px) {
        padding-top: 90px;
        margin-top: -30px;
    }
`

const Title = styled.h2`
    font-size: clamp(28px, 3.5vw, 45px);
    line-height: clamp(40px, 3.5vw, 60px);
    font-weight: bold;
    max-width: 1080px;
    text-align: center;
    margin: 0 auto 120px;

    @media(max-width: 1198px) {
        margin: 0 auto 55px;
        font-size: 28px;
        line-height: 40px;
    }

    @media(max-width: 764px) {
        margin: 0 auto 60px;
    }
`

const List = styled.ul`
    li:nth-child(odd){
        flex-direction: row;

        @media(max-width: 1198px) {
            flex-direction: column;
        }

        img {
            margin-right: 15px;
            max-width: calc(100% - 15px);
            @media(max-width: 1199px) {
           margin-right: 0;
           max-width: 100%;
        }
        }
    }

    li:nth-child(even){
      

        img {
            margin-left: 15px;
            max-width: calc(100% - 15px);

        @media(max-width: 1199px) {
           margin-left: 0;
           max-width: 100%;
        }
            
        }
    }

`

const ListItem = styled.li`
    display: flex;
    justify-content: space-between;
    align-items: center;
    flex-direction: row-reverse;
    margin-top: 120px;
    position: relative;
    @media(max-width: 1199px) {
            flex-direction: column;
            :first-child {
                margin-top: 0;
            }
            max-width: 65vw;
            margin: 60px auto 0;
        }

        @media (max-width:767px) {
            margin-top: 45px;
            max-width: 100%;
        }
   
`

const ImgContener = styled.div`
width: 50%;

    @media(max-width: 1199px) {
            width: 100%;
            margin-bottom: 30px;
        }
`

const Img = styled.img`
    max-width: 100%;
    width: 100%;
   
`

const TextWrapper = styled.div`
    width: 665px;
    @media(max-width: 1199px) {
            width: 100%;
        }
`

const ListTitle = styled.h3`
    font-size: 40px;
    font-weight: bold;
    line-height: 50px;
    @media (max-width:1199px) {
        font-size: 28px;
        line-height: 40px;
        margin-bottom: 15px; 
    }
    @media (max-width:767px) {
        font-size: 18px;
        line-height: 24px;
    }
`

const ListSubTitle = styled.p`
    padding: 20px 0 15px;
    font-size: 20px;
    color: #6E7276;
    line-height: 28px;
    @media (max-width:1199px) {
        font-size: 18px;
        line-height: 24px;
        padding: 0;
    }
    @media (max-width:767px) {
        font-size: 14px;
        line-height: 20px;
    }
    `



const Callback = (props) => {
    return (
        <Article id={props.acf.id}>
            <Container>
                <Title>{props.acf.title}</Title>
                <List>
                    {
                        props.acf.repeater.map((el, index) =>
                            <ListItem key={index}>
                                <ImgContener>
                                    <Img alt={el.img_alt} src={el.img} />
                                </ImgContener>
                                <TextWrapper>
                                    <ListTitle>{el.title_repeater}</ListTitle>
                                    <ListSubTitle>{el.text_repeater}</ListSubTitle>
                                </TextWrapper>
                            </ListItem>
                        )
                    }
                </List>
            </Container>
        </Article>
    )
}

export default Callback