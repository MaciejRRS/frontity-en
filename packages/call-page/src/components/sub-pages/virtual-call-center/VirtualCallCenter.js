import React from 'react'
import { connect } from "frontity"
import Hero from './components/Hero'
import Content from './components/Content'
import Integration from './../../../../common/bricks/Integration'

const VirtualCallCenter = ({ state }) => {

    let props
    const data = state.source.get(state.router.link)
    if (data.isReady) {
        props = state.source[data.type][data.id]
    }

    return (
        <main className='body'>
            {data.isReady
                ? <>
                    <Hero acf={props.acf.hero} form_free_test={props.acf.form_free_test} />
                    <Content acf={props.acf.content} />
                    <Integration acf={props.acf.integration} />
                </>
                : null
            }
        </main>
    )
}

export default connect(VirtualCallCenter)