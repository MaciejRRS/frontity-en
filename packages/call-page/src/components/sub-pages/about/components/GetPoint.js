import React from 'react'
import { Container } from '../../../../../common/styles'
import { styled } from "frontity"
import Background from '../../../../../common/sprites/concrete_bg.png'

const Article = styled.article`
    position: relative;
    padding-top: ${props => props.marginDesktop ? props.marginDesktop : 'clamp(140px, 12vw, 240px);'};
    @media(max-width: 1198px){
        padding-top: ${props => props.marginTablet ? props.marginTablet : 'clamp(90px, 12vw, 140px);'};
    }
    @media(max-width: 764px){
        padding-top: ${props => props.marginPhone ? props.marginPhone : '90px'};
    }
`


const BottomBG = styled.div`
    background-image: url(${Background});
    background-repeat: no-repeat;
    background-size: contain;
    background-position: center;
    width: 100%;
    height: 340px;
    display: flex;
    align-items: center;
    @media(max-width: 1198px){
        background-size: cover;
    }
    @media(max-width: 764px){
        background-image: unset;
        height: auto;
    }
`

const Title = styled.h2`
    font-size: 45px;
    line-height: 60px;
    font-weight: bold;
    @media(max-width: 1198px){
        font-size: 28px;
        line-height: 40px;
    }
    @media(max-width: 764px){
        font-size: 22px;
        line-height: 30px;
    }
`

const SubTitle = styled.h3`
    font-size: 28px;
    line-height: 40px;
    padding: 20px 0 30px;
    @media(max-width: 1198px){
        font-size: 22px;
        line-height: 30px;
    }
    @media(max-width: 764px){
        font-size: 18px;
        line-height: 28px;
    }
`

const Text = styled.p`
    font-size: 22px;
    line-height: 30px;
    color: #6E7276;
    max-width: 605px;
    margin: 0 auto;
    @media(max-width: 1198px){
        font-size: 16px;
        line-height: 24px;
    }
    @media(max-width: 764px){
        font-size: 14px;
        line-height: 20px;
    }
`

const Item = styled.div`
    max-width: 804px;
    width: 100%;
    padding: 60px 30px;
    box-sizing: border-box;
    text-align: center;
    background-color: #fff;
    box-shadow: 0 3px 6px 0 #00000016;
    margin: 0 auto -30px;
    @media(max-width: 764px){
        padding: 30px;
    }
`

const GetPoint = (props) => {
    return (
        <Article marginDesktop={props.marginDesktop} marginTablet={props.marginTablet} marginPhone={props.marginPhone}>
            <BottomBG >
                <Container>
                    <Item>
                        <Title>{props.acf.title}</Title>
                        <SubTitle>{props.acf.sub_title}</SubTitle>
                        <Text>{props.acf.text}</Text>
                    </Item>
                </Container>
            </BottomBG>
        </Article>
    )
}

export default GetPoint