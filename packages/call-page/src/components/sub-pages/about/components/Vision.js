import React from 'react'
import { Container } from '../../../../../common/styles'
import { styled } from "frontity"
import Background from '../../../../../common/sprites/bg_features.png'

const Article = styled.article`
    position: relative;
    padding-top: ${props => props.marginDesktop ? props.marginDesktop : 'clamp(140px, 12vw, 240px);'};
    @media(max-width: 1198px){
        padding-top: ${props => props.marginTablet ? props.marginTablet : 'clamp(90px, 12vw, 140px);'};
    }
    @media(max-width: 764px){
        padding-top: ${props => props.marginPhone ? props.marginPhone : '90px'};
    }
`

const BottomBG = styled.div`
    background-image: url(${Background});
    background-repeat: no-repeat;
    background-size: cover;
    background-position: 80%;
    width: 100%;
    height: 360px;
    display: flex;
    align-items: center;
`

const Title = styled.h2`
    text-align: center;
    font-size: clamp(45px, 3.5vw, 60px);
    line-height: clamp(60px, 3.5vw, 80px);
    font-weight: bold;
    margin-bottom: 10px;
    @media(max-width: 1198px){
        font-size: 45px;
        line-height: 60px;
    }
    @media(max-width: 764px){
        font-size: 28px;
        line-height: 40px;
    }
`

const Text = styled.p`
    text-align: center;
    font-size: 28px;
    font-weight: bold;
    line-height: 45px;
    color: #FFFFFF;
    @media(max-width: 1198px){
        font-size: 22px;
        line-height: 40px;
    }
    @media(max-width: 764px){
        font-size: 18px;
        line-height: 30px;
    }

    @media(max-width: 450px){
        font-size: 16px;
        line-height: 28px;
    }
`

const LocContainer = styled(Container)`
    display: flex;
    align-items: center;
`

const Vision = (props) => {

    return (
        <Article>
            <Container>
                <Title>{props.acf.title}</Title>
            </Container>
            <BottomBG>
                <LocContainer>
                    <Text>{props.acf.text}</Text>
                </LocContainer>
            </BottomBG>
        </Article>
    )
}

export default Vision