import React from 'react'
import { Container, Flex } from '../../../../../common/styles'
import { styled } from "frontity"

const Article = styled.article`
    position: relative;
    padding-top: ${props => props.marginDesktop ? props.marginDesktop : 'clamp(140px, 12vw, 240px);'};
    @media(max-width: 1198px){
        padding-top: ${props => props.marginTablet ? props.marginTablet : 'clamp(90px, 12vw, 140px);'};
    }
    @media(max-width: 764px){
        padding-top: ${props => props.marginPhone ? props.marginPhone : '90px'};
    }
`

const Title = styled.h2`
    text-align: center;
    font-size: 45px;
    line-height: 60px;
    font-weight: bold;
    padding: 0 0 100px;
    @media(max-width: 1198px){
        font-size: 28px;
        line-height: 40px;
        padding: 0 0 60px;
    }  
    @media(max-width: 764px){
        font-size: 22px;
        line-height: 30px;
        padding: 0 0 40px;
    }
`

const Text = styled.p`
    text-align: center;
    font-weight: bold;
`

const LocFlex = styled(Flex)`
    flex-wrap: wrap;
    justify-content: space-evenly;
    align-items: flex-start;
`

const Item = styled.div`
    flex-basis: 220px;
    padding: 0 5px 30px;
    img{
        width: 100%;
        max-width: 150px;
        display: block;
        margin: 0 auto 20px;
    }


    @media(max-width: 767px){
        flex-basis: 40%;
    }

    @media(max-width: 500px){
        flex-basis: 100%;
    }
`

const CreatedFor = (props) => {
    return (
        <Article>
            <Container>
                <Title>{props.acf.title}</Title>
                <LocFlex>
                    {
                        props.acf.repeater.map((el, index) =>
                            <Item key={index}>
                                <img alt={el.img_alt} src={el.img} />
                                <Text>{el.text}</Text>
                            </Item>
                        )
                    }
                </LocFlex>
            </Container>
        </Article>
    )
}

export default CreatedFor