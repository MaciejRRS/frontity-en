import React from 'react'
import { styled } from "frontity"
import { Container } from '../../../../../common/styles'
import NavLink from "@frontity/components/link"

const Article = styled.article`
    padding-top: ${props => props.marginDesktop ? props.marginDesktop : '120px'};
    @media(max-width: 1198px){
        padding-top: ${props => props.marginTablet ? props.marginTablet : '90px'};
    }
    @media(max-width: 764px){
        padding-top: ${props => props.marginPhone ? props.marginPhone : '60px'};
    }
`

const Grid = styled.div`
    display: grid;
    grid-template-columns: repeat(10, 1fr);
    grid-row-gap: 30px;
    grid-column-gap: 30px;
    @media(max-width: 1560px){
        grid-template-columns: repeat(8, 1fr);
    }
    @media(max-width: 1260px){
        grid-template-columns: repeat(6, 1fr);
    }
    @media(max-width: 980px){
        grid-template-columns: repeat(5, 1fr);
    }
    @media(max-width: 840px){
        grid-template-columns: repeat(4, 1fr);
    }
    @media(max-width: 710px){
        grid-template-columns: repeat(3, 1fr);
        grid-row-gap: 15px;
        grid-column-gap: 15px;
    }
    @media(max-width: 420px){
        grid-template-columns: repeat(2, 1fr);
    }

`

const Item = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: space-between;

    img{
        max-width: 36px;
    }
`

const ItemText = styled.h3`
    font-size: 14px;
    line-height: 22px;
    font-weight: 400;
    padding-top: 5px;
`

const Title = styled.h2`
    text-align: center;
    padding-bottom: 40px;
    font-size: 48px;
    line-height: 68px;
    font-weight: 700;
    max-width: 760px;
    margin: 0 auto;
    @media(max-width: 1198px){
        font-size: 45px;
        line-height: 60px;

    }
    @media(max-width: 764px){
        font-size: 28px;
        line-height: 40px;
    }
`

const Content = styled.div`
    box-sizing: border-box;
    padding: 30px;
    box-shadow: 0 3px 6px 0 #00000016;
    border-radius: 6px;
    max-width: 1082px;
    margin: 0 auto;
    @media(max-width: 480px){
        padding: 30px 15px;
    }
`

const Link = styled(NavLink)`
    color: #377DFF;
`

const Annotation = styled.p`
    max-width: 604px;
    margin: 0 auto;
    text-align: center;
    padding-top: 40px;
    font-size: 16px;
    line-height: 26px;
    font-weight: 500;
    @media(max-width: 1198px){
        padding-top: 30px;

    }
    @media(max-width: 768px){
        padding-top: 20px;

    }
`

const Countries = (props) => {
    return (
        <>
            {props.acf.is_hidden
                ? null
                : <Article>
                    <Container>
                        <Title>{props.acf.title}</Title>
                        <Content>
                            <Grid>
                                {props.acf.repeater.map((el, index) =>
                                    <Item key={index}>
                                        <img alt={el.flag_alt} src={el.flag} />
                                        <ItemText children={el.text} />
                                    </Item>
                                )}
                            </Grid>
                            <Annotation>
                                {props.acf.annotation.text}
                                {props.acf.annotation.link_type
                                    ? <Link link={props.acf.annotation.link_url} children={props.acf.annotation.link_text} />
                                    : <Link as='a' rel="noreferrer" target='_blank' href={props.acf.annotation.link_url} children={props.acf.annotation.link_text} />
                                }
                            </Annotation>
                        </Content>
                    </Container>
                </Article>
            }

        </>
    )
}

export default Countries