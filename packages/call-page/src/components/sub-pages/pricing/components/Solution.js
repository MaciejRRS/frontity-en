import React from 'react'
import { styled } from "frontity"
import { Container, Flex } from '../../../../../common/styles'

const Article = styled.article`
    padding-top: ${props => props.marginDesktop ? props.marginDesktop : '120px'};
    @media(max-width: 1198px){
        padding-top: ${props => props.marginTablet ? props.marginTablet : '90px'};
    }
    @media(max-width: 764px){
        padding-top: ${props => props.marginPhone ? props.marginPhone : '60px'};
    }
`

const LocFlex = styled(Flex)`
    justify-content: space-evenly;
    @media(max-width: 1198px){
        flex-direction: column-reverse;
    }
`

const TextPart = styled.div`
    max-width: 526px;
    margin-right: 30px;
    @media(max-width: 1198px){
        margin-right: 0;
        margin-top: 60px;
    }
`

const ImgPart = styled.div`
    max-width: 526px;

    img{
        width: 100%;
    }
`

const Title = styled.h2`
    font-size: 24px;
    line-height: 36px;
    font-weight: 700;
    padding-bottom: 40px;
    @media(max-width: 1198px){
        padding-bottom: 25px;
    }
    @media(max-width: 764px){
        font-size: 22px;
        line-height: 30px;
    }
`

const Text = styled.p`
    font-size: 16px;
    line-height: 22px;
    font-weight: 400;
    padding-bottom: 30px;
    @media(max-width: 1198px){
        font-size: 14px;
        line-height: 20px;
        padding-bottom: 20px;
    }
    @media(max-width: 764px){

    }
`

const Grid = styled.div`
    display: grid;
    grid-template-columns: 1fr 1fr 1fr;
    grid-column-gap: 15px;
`

const Item = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: space-between;
    text-align: center;
    img{
        max-width: 120px;
        max-height: 120px;
    }

    @media(max-width: 764px){
        img{
            max-width: 90px;
            max-height: 90px;
        }
    }

    h3{
        font-size: 16px;
        line-height: 22px;
        font-weight: 600;
        padding-bottom: 30px;
        @media(max-width: 764px){
            font-size: 14px;
            line-height: 20px;  
        }   
    }
`

const Solution = (props) => {
    return (
        <Article>
            <Container>
                <LocFlex>
                    <TextPart>
                        <Title>{props.acf.text_title}</Title>
                        <Text>{props.acf.text}</Text>
                        <Grid>
                            {props.acf.repeater.map((el, index) =>
                                <Item key={index}>
                                    <h3>{el.title}</h3>
                                    <img alt={el.img_alt} src={el.img} />
                                </Item>
                            )}
                        </Grid>
                    </TextPart>
                    <ImgPart>
                        <img alt={props.acf.img_alt} src={props.acf.img} />
                    </ImgPart>
                </LocFlex>
            </Container>
        </Article>
    )
}

export default Solution