import React from 'react'
import { styled } from "frontity"
import { Container } from '../../../../common/styles'
import ContentItem from './ContentItem'
import NavLink from "@frontity/components/link"

const Grid = styled.div`
    display: grid;
    grid-template-columns: 1fr 1fr 1fr;
    grid-column-gap: 60px;
    grid-row-gap: 60px;
    max-width: 984px;
    margin: 0 auto;
    @media (max-width: 1096px){
        grid-template-columns: 1fr 1fr;
    }
    @media(max-width: 764px){
        grid-template-columns: 1fr;
    }
`

const ButtonPagination = styled(NavLink)`
    padding: 0 5px;
    margin: 0 5px;
    color: ${props => props.page ? '#377DFF' : '#000000'};
    border: none;
    background-color: transparent;
    cursor: ${props => props.page ? 'default' : 'pointer'};
    font-size: 22px;

    &:disabled{
        cursor: default;
        color: #C2C9D1;
    }
`

const ButtonPaginationDisabled = styled.button`
    font-size: 22px;
    padding: 0 5px;
    margin: 0 5px;
    color: ${props => props.page ? '#377DFF' : '#000000'};
    border: none;
    background-color: transparent;
    cursor: ${props => props.page ? 'default' : 'pointer'};

    &:disabled{
        cursor: default;
        color: #C2C9D1;
    }
`

const Pagination = styled.div`
    padding: ${props => props.bottom ? '30px 0 0 0' : '0 0 30px 0'};
    display: flex;
    align-items: center;
    justify-content: center;
`

const Content = (props) => {
    debugger
    return (
        <article>
            <Container>
                {props.ebooks.previous
                    ? <Pagination>
                        {
                            props.ebooks.previous
                                ? <ButtonPagination link={props.ebooks.previous}>{'<'}</ButtonPagination>
                                : <ButtonPaginationDisabled disabled >{'<'}</ButtonPaginationDisabled>
                        }
                        <ButtonPaginationDisabled page>{props.ebooks.page}</ButtonPaginationDisabled>
                        {
                            props.ebooks.next
                                ? <ButtonPagination link={props.ebooks.next}>{'>'}</ButtonPagination>
                                : <ButtonPaginationDisabled disabled>{'>'}</ButtonPaginationDisabled>
                        }
                    </Pagination>
                    : props.ebooks.next
                        ? <Pagination>
                            {
                                props.ebooks.previous
                                    ? <ButtonPagination link={props.ebooks.previous}>{'<'}</ButtonPagination>
                                    : <ButtonPaginationDisabled disabled >{'<'}</ButtonPaginationDisabled>
                            }
                            <ButtonPaginationDisabled page>{props.ebooks.page}</ButtonPaginationDisabled>
                            {
                                props.ebooks.next
                                    ? <ButtonPagination link={props.ebooks.next}>{'>'}</ButtonPagination>
                                    : <ButtonPaginationDisabled disabled>{'>'}</ButtonPaginationDisabled>
                            }
                        </Pagination>
                        : null
                }
                <Grid>
                    {
                        props.ebooks.items.map((el, index) =>
                            <ContentItem key={index} props={el} state={props.state} />
                        )
                    }
                </Grid>
                {props.ebooks.previous
                    ? <Pagination bottom>
                        {
                            props.ebooks.previous
                                ? <ButtonPagination link={props.ebooks.previous}>{'<'}</ButtonPagination>
                                : <ButtonPaginationDisabled disabled >{'<'}</ButtonPaginationDisabled>
                        }
                        <ButtonPaginationDisabled page>{props.ebooks.page}</ButtonPaginationDisabled>
                        {
                            props.ebooks.next
                                ? <ButtonPagination link={props.ebooks.next}>{'>'}</ButtonPagination>
                                : <ButtonPaginationDisabled disabled>{'>'}</ButtonPaginationDisabled>
                        }
                    </Pagination>
                    : props.ebooks.next
                        ? <Pagination bottom>
                            {
                                props.ebooks.previous
                                    ? <ButtonPagination link={props.ebooks.previous}>{'<'}</ButtonPagination>
                                    : <ButtonPaginationDisabled disabled >{'<'}</ButtonPaginationDisabled>
                            }
                            <ButtonPaginationDisabled page>{props.ebooks.page}</ButtonPaginationDisabled>
                            {
                                props.ebooks.next
                                    ? <ButtonPagination link={props.ebooks.next}>{'>'}</ButtonPagination>
                                    : <ButtonPaginationDisabled disabled>{'>'}</ButtonPaginationDisabled>
                            }
                        </Pagination>
                        : null
                }
            </Container>
        </article>
    )
}

export default Content