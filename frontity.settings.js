const settings = {
  "name": "call-page",
  "state": {
    "frontity": {
      "url": "https://wp.callpage.io",
      "title": "CallPage",
      "description": "Turn website visitors into sales calls, instantly",
    }
  },
  "packages": [
    {
      "name": "call-page",
    },
    {
      "name": "@frontity/wp-source",
      "state": {
        "source": {
          "url": "https://wp.callpage.io",
          params: {
            per_page: 9,
          },
          "postTypes": [
            {
              type: "blog",
              endpoint: "blog",
              archive: "/blog"
            },
            {
              type: "ebooks",
              endpoint: "ebooks",
              archive: "/ebooks",
            },
            {
              type: "casestudies",
              endpoint: "casestudies",
              archive: "/casestudies"
            },
            {
              type: "industries",
              endpoint: "industries",
              archive: "/industries"
            }
          ],
          taxonomies: [
            {
              taxonomy: "category_casestudies",
              endpoint: "category_casestudies",
              postTypeEndpoint: "casestudies",
              params: {
                per_page: 10,
                _embed: true,
              },
            },
            {
              taxonomy: "category_blog",
              endpoint: "category_blog",
              postTypeEndpoint: "blog",
              params: {
                per_page: 10,
                _embed: true,
              },
            },
          ]
        }
      },
    },
    {
      name: "@frontity/google-tag-manager-analytics",
      state: {
        googleTagManagerAnalytics: {
          containerIds: ["GTM-PPFXRVZ"],
        },
      },
    },
    // {
    //   name: "@frontity/google-analytics",
    //   state: {
    //     googleAnalytics: {
    //       trackingId: "UA-202531233-1",
    //     },
    //   },
    // },
    "@frontity/tiny-router",
    "@frontity/html2react"
  ]
};

export default settings;
